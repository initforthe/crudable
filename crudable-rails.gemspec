# frozen_string_literal: true

require_relative 'lib/crudable/rails/version'

Gem::Specification.new do |spec|
  spec.name        = 'crudable-rails'
  spec.version     = Crudable::Rails::VERSION
  spec.authors     = ['Tomislav Simnett']
  spec.email       = ['tom@initforthe.com']
  spec.homepage    = 'https://gitlab.com/initforthe/crudable-rails'
  spec.summary     = 'CRUD operations for Rails controllers'
  spec.description = 'Crudable Rails provides everything needed to quickly build fully functional CRUD-based controllers in a Rails application. It streamlines resource management, reduces boilerplate, and enforces best practices for handling requests, responses, and strong parameters. Ideal for developers looking to accelerate Rails development while maintaining flexibility and clean architecture.'
  spec.license = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the "allowed_push_host"
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  # spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"

  # spec.metadata['homepage_uri'] = spec.homepage
  # spec.metadata['source_code_uri'] = "TODO: Put your gem's public repo URL here."
  # spec.metadata['changelog_uri'] = "TODO: Put your gem's CHANGELOG.md URL here."

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']
  end

  spec.required_ruby_version = '>= 3.0.0'

  spec.add_dependency 'rails', '>= 7.0', '< 9.0.0'
  spec.add_dependency 'turbo-rails', '~> 2.0.0'
  spec.add_development_dependency 'rubocop', '>= 1.50', '< 2.0'
  spec.add_development_dependency 'mocha', '>= 2.6', '< 3.0.0'
end

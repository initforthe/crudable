# frozen_string_literal: true

class ProductSizesController < ApplicationController
  crudable nested: true
end

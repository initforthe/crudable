# frozen_string_literal: true

module Admin
  class ProductsController < ApplicationController
    crudable
  end
end

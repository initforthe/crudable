# frozen_string_literal: true

class ProductsController < ApplicationController
  crudable

  private

  def permitted_params
    [:name]
  end
end

require 'test_helper'

class CrudableInclusionTest < ActionDispatch::IntegrationTest
  fixtures :products

  setup do
    @product = products(:one)
  end

  def test_renders_crudable_controller_index
    get products_path
    assert_response :success
  end

  def test_renders_nestable_controller_index
    product = Product.create(name: 'Test Product')
    product.product_sizes.create(name: 'Test Size')
    get product_product_sizes_path(product)
    assert_response :success
  end

  def test_renders_namespaced_controller_index
    get admin_products_path
    assert_response :success
  end

  def test_after_update_redirect_path
    patch product_path(@product), params: { product: { name: 'Updated Name' } }
    assert_redirected_to "/products/#{@product.id}"
  end

  def test_after_update_notice
    patch product_path(@product), params: { product: { name: 'Updated Name' } }
    follow_redirect!
    assert_equal 'Product successfully updated', flash[:success]
  end

  def test_after_destroy_redirect_path
    delete product_path(@product)
    assert_redirected_to "/products"
  end

  def test_after_failed_destroy_redirect_path
    # Simulate a failed destroy action
    Product.any_instance.stubs(:destroy).returns(false)
    assert_no_difference('Product.count') do
      delete product_path(@product)
    end
    assert_redirected_to "/products"
  end

  def test_after_destroy_notice
    delete product_path(@product)
    follow_redirect!
    assert_equal 'Product successfully destroyed', flash[:success]
  end

  def test_after_failed_destroy_alert
    # Simulate a failed destroy action
    Product.any_instance.stubs(:destroy).returns(false)
    assert_no_difference('Product.count') do
      delete product_path(@product)
    end
    follow_redirect!
    assert_equal 'Product could not be destroyed', flash[:alert]
  end

  def test_on_successful_create
    post products_path, params: { product: { name: 'New Product' } }
    follow_redirect!
    assert_equal 'Product successfully created', flash[:success]
  end

  def test_on_successful_create_render
    post products_path, params: { product: { name: 'New Product' } }
    assert_redirected_to "/products"
  end

  def test_on_failed_create_setup
    post products_path, params: { product: { name: '' } } # Assuming name is required
    assert_response :unprocessable_entity
  end

  def test_on_successful_update
    patch product_path(@product), params: { product: { name: 'Updated Name' } }
    follow_redirect!
    assert_equal 'Product successfully updated', flash[:success]
  end

  def test_on_successful_update_render
    patch product_path(@product), params: { product: { name: 'Updated Name' } }
    assert_redirected_to "/products/#{@product.id}"
  end

  def test_on_failed_update_setup
    patch product_path(@product), params: { product: { name: '' } } # Assuming name is required
    assert_response :unprocessable_entity
  end

  def test_on_successful_destroy_render
    delete product_path(@product)
    assert_redirected_to "/products"
  end
end

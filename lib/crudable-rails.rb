require 'turbo-rails'

require 'crudable/rails/version'
require 'crudable/rails/engine'
require 'crudable/rails/controller'
require 'crudable/rails/base'
require 'crudable/rails/resourceable'
require 'crudable/rails/nestable'

module Crudable
  module Rails
    def self.deprecator
      @deprecator ||= ActiveSupport::Deprecation.new("2.0", "Crudable::Rails")
    end
  end
end

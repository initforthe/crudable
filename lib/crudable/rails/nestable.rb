# frozen_string_literal: true

module Crudable
  module Rails
    # Nestable Module
    module Nestable
      extend ActiveSupport::Concern

      included do
        before_action :find_parent, only: %i[index new create]
      end

      def find_parent
        params.each do |name, value|
          next unless name =~ /(.+)_id$/

          object_name = Regexp.last_match(1)
          object_scope = object_name.classify.constantize
          object_scope = object_scope.friendly if friendly_finders?
          instance_variable_set("@#{object_name}", object_scope.find(value))
          self.class.send(:decorates_assigned, object_name.to_sym) if defined?(Draper)
        end
      end
    end
  end
end

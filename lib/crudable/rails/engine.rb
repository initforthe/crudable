module Crudable
  module Rails
    class Engine < ::Rails::Engine
      ActiveSupport.on_load(:action_controller) do
        ::ActionController::Base.include Crudable::Rails::Controller
      end

      initializer "crudable-rails.deprecator" do |app|
        app.deprecators[:crudable_rails] = Crudable::Rails.deprecator
      end
    end
  end
end

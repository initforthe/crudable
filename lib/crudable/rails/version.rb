# frozen_string_literal: true

# Crudable Version
module Crudable
  module Rails
    VERSION = '1.2.1'
  end
end

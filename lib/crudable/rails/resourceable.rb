# frozen_string_literal: true

module Crudable
  module Rails
    # Resourceable
    #
    # Provides a number of resource helpers for controllers supporting Crudable
    module Resourceable
      extend ActiveSupport::Concern

      def resource_name
        self.class.name.split('::').last.gsub('Controller', '').singularize
      end

      def resource_human_name
        resource_name.humanize
      end

      def resource_class
        resource_name.constantize
      end

      def resource_var_name
        resource_name.underscore
      end

      def plural_resource_var_name
        resource_name.pluralize.underscore
      end

      def resource_namespace
        namespaces = self.class.name.split('::')
        namespaces.pop
        namespaces.map { |n| n.downcase.to_sym }
      end

      def authorize_resource(method = action_name)
        authorize resource_namespace + [authorizable_resource], "#{method}?".to_sym
      end

      def authorizable_resource
        instance_variable_get("@#{resource_var_name}")
      end

      class_methods do
        def resource_name
          name.split('::').last.gsub('Controller', '').singularize
        end

        def resource_class
          resource_name.constantize
        end

        def resource_var_name
          resource_name.underscore
        end

        def plural_resource_var_name
          resource_name.pluralize.underscore
        end

        def resource_namespace
          namespaces = name.split('::')
          namespaces.pop
          namespaces.map { |n| n.underscore.to_sym }
        end
      end
    end
  end
end

# frozen_string_literal: true

module Crudable
  module Rails
    module Base
      extend ActiveSupport::Concern

      included do
        include Crudable::Rails::Resourceable

        before_action :find_resource, only: %i[show edit update destroy]
        after_action :authorize_resource, only: %i[new show edit] if defined?(Pundit)

        decorates_assigned plural_resource_var_name.to_sym, resource_var_name.to_sym if defined?(Draper)
      end

      def index
        instance_variable_set("@#{plural_resource_var_name}", resource_scope)
        paginate_resource
      end

      def show; end

      def new
        instance_variable_set("@#{resource_var_name}", resource_class.new)
      end

      def create # rubocop:disable Metrics/MethodLength
        instance_variable_set("@#{resource_var_name}", resource_class.new(create_params)) unless skip_initialize_create?
        if defined?(Pundit)
          before_authorize_create
          authorize_resource
          after_authorize_create
        end
        if instance_variable_get("@#{resource_var_name}").save
          on_successful_create
          on_successful_create_render
        else
          on_failed_create_setup
          on_failed_create_render
        end
      end

      def edit; end

      def update
        if defined?(Pundit)
          before_authorize_update
          authorize_resource
          after_authorize_update
        end
        if instance_variable_get("@#{resource_var_name}").update update_params
          on_successful_update
          on_successful_update_render
        else
          on_failed_update_setup
          on_failed_update_render
        end
      end

      def destroy
        authorize_resource(destroy_method) if defined?(Pundit)
        if instance_variable_get("@#{resource_var_name}").send(destroy_method)
          on_successful_destroy
          on_successful_destroy_render
        else
          on_failed_destroy
          on_failed_destroy_render
        end
      end

      private

      def destroy_method
        return :destroy unless defined?(Discard)
        return :destroy if params[:destroy]

        @destroy_method ||= discard? && !instance_variable_get("@#{resource_var_name}").discarded? ? :discard : :destroy
      end

      def discard?
        false
      end

      def find_resource
        resource = friendly_finders? ? resource_class.friendly : resource_class
        instance = singleton? ? resource.first : resource.find(params[finder_param])

        return redirect_to (resource_namespace + [instance]), status: :moved_permanently if should_redirect?(instance)

        instance_variable_set("@#{resource_var_name}", instance)
      end

      def should_redirect?(instance)
        friendly_finders? && request.path != polymorphic_path(resource_namespace + [instance])
      end

      def singleton?
        false
      end

      def finder_param
        :id
      end

      def resource_scope
        return apply_scopes(authorizable_scope) if defined?(HasScope)

        authorizable_scope
      end

      def authorizable_scope
        defined?(Pundit) ? policy_scope(resource_namespace + [resource_class]) : resource_class.all
      end

      def paginate_resource?
        return false unless defined?(Kaminari)

        true
      end

      def friendly_finders?
        return false unless defined?(FriendlyId) && resource_class.respond_to?(:friendly)

        true
      end

      def skip_initialize_create?
        false
      end

      def paginate_resource
        return unless paginate_resource?

        instance_variable_set(
          "@#{plural_resource_var_name}",
          instance_variable_get("@#{plural_resource_var_name}").page(params[:page]).per(params[:per])
        )
      end

      def after_create_redirect_path
        resource_namespace + [plural_resource_var_name.to_sym]
      end

      def after_create_notice
        t('crudable.created', model_name: resource_class.model_name.human)
      end

      def after_update_redirect_path
        resource_namespace + [instance_variable_get("@#{resource_var_name}")]
      end

      def after_update_notice
        t('crudable.updated', model_name: resource_class.model_name.human)
      end

      def after_destroy_redirect_path
        resource_namespace + [plural_resource_var_name.to_sym]
      end

      def after_failed_destroy_redirect_path
        after_destroy_redirect_path
      end

      def after_destroy_notice
        t('crudable.destroyed', model_name: resource_class.model_name.human)
      end

      def after_failed_destroy_alert
        t('crudable.not_destroyed', model_name: resource_class.model_name.human)
      end

      def on_successful_create
        flash[:success] = after_create_notice
      end

      def on_successful_create_render
        redirect_to after_create_redirect_path
      end

      def on_failed_create_setup; end

      def on_successful_update
        flash[:success] = after_update_notice
      end

      def on_successful_update_render
        redirect_to after_update_redirect_path
      end

      def on_failed_update_setup; end

      def on_successful_destroy
        flash[:success] = after_destroy_notice
      end

      def on_failed_destroy
        flash[:alert] = after_failed_destroy_alert
      end

      def on_successful_destroy_render
        redirect_to after_destroy_redirect_path
      end

      def on_failed_destroy_render
        redirect_to after_failed_destroy_redirect_path
      end

      def before_authorize_create; end

      def before_authorize_update; end

      def after_authorize_create; end

      def after_authorize_update; end

      def on_failed_create_render
        respond_to do |format|
          format.turbo_stream { render_action(:new) }
          format.html { render :new, status: :unprocessable_entity }
        end
      end

      def on_failed_update_render
        respond_to do |format|
          format.turbo_stream { render_action(:edit) }
          format.html { render :edit, status: :unprocessable_entity }
        end
      end

      def create_params
        resource_params
      end

      def update_params
        resource_params
      end

      def resource_params
        params.require(resource_var_name).permit(*permitted_params)
      end

      def permitted_params
        raise NotImplementedError, 'You must implement permitted_params in your controller'
      end

      def render_action(default_action_name)
        logger.debug "Rendering relevant action for #{controller_path}/#{default_action_name} as #{request.format.symbol}"
        return render default_action_name if lookup_context.template_exists?("#{controller_path}/#{default_action_name}")

        Crudable::Rails.deprecator.warn("Rendering fallback: #{action_name}, format: #{request.format.symbol}. Rename your template to #{default_action_name}")

        logger.debug "Rendering fallback: #{action_name}"
        render
      end
    end
  end
end

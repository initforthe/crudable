# frozen_string_literal: true

module Crudable
  module Rails
    # Include Crudable Controller Class Methods
    module Controller
      extend ActiveSupport::Concern

      class_methods do
        def crudable(nested: false)
          include Crudable::Rails::Base
          include Crudable::Rails::Nestable if nested
        end
      end
    end
  end
end
